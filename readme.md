Сервис доступен (при включенном проекте) через rodionkudryavtsev.ru:8080

## Как это работает

1. Terraform'ом создаются 5 вм (ноды сервисов):
 - prod-1. 1-ая нода продуктового сервера/сервиса rodionkudryavtsev.ru:8080
 - prod-2. 2-ая нода продуктового сервера/сервиса rodionkudryavtsev.ru:8080
 - test-1. Нода тестового сервера test.rodionkudryavtsev.ru:8080
 - monitoring-1. monitoring.rodionkudryavtsev.ru:3000
 - ci-1. Сервер гитлаб-раннеров. Это служебный сервер - отдельная dns-запись ему не нужна
	Terraform генерирует сервер при помощи модулей.
	Абстракция сервер = instance (node) + load balancer + dns_recordset

2. Ansible-ролями настраиваются все машины. Для всех есть общая роль (commom_actions). Затем к группе нод определенного сервиса применяются уже специфические роли согласно назначению этого сервера в проекте.
3. Сервер мониторинга - docker compose поднимает grafana с преднастроенными datasource - prometheus, loki и дашбордами.
4. В репозитории https://gitlab.com/rodion_kudryavtsev/skillbox-diploma.git написан .gitlab-ci.yml. сi-1, определенный раннером в рамках этого проекта, выполняет ansible-playbook test.yml, ansible-playbook prod.yml. Т.е. на раннере установлен ansible, который запускает плейбуки для test/prod.


## Общая часть

Сервис отвечает по 3 эндпоинтам:

- /health - 200 ok
- /metrics - в формате метрик для prometheus, включая счётчик запросов в основной эндпоинт skillbox_http_requests_total
- / - основной эндпоинт, возвращающий часть запроса и генерирующий строчку лога.

## Инфраструктура
Репозиторий содержит terraform-файл, описывающий создаваемую инфраструктуру, разворачиваемую в Yandex.Cloud
Инфраструктура включает в себя:
- 5 ВМ (instances)
- DNS-зону rodionkudryavtsev.ru
- DNS-записи
	@ (self-domain record),
	test,
	monitoring,
	grafana
- статические публичные адреса для доступности "извне"
- приватную сеть 192.168.0.0/24
- балансировщики нагрузки с привязанным к ним публичным адресам


