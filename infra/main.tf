terraform {
    required_providers {
        yandex = {
            source = "yandex-cloud/yandex"
        }
    }
}
provider "yandex" {
    zone = var.yc_zone
}

#===========================================
# module srv = instance + lb + dns_recordset

module "dns" {

	source						= "./modules/dns"

	yc_zone						= var.yc_zone

	proj_dns_zone				= "rodionkudryavtsev.ru."	# zone name must ends with '.'
}

module "network" {

	source						= "./modules/network"

	yc_zone						= var.yc_zone

	proj_ext_ip_count			= 4						# сколько публичных адресов просим выделить (= кол-ву dns_recordset)
	proj_loc_subnets			= [						# какие подсети делаем
									"192.168.0.0/24"
								]


}

module "srv-ci" {

	source						= "./modules/server"

	yc_zone						= var.yc_zone

	srv_dns_domain				= module.dns.proj_domain

	node_platform_id 			= "standard-v3"
	node_name_preffix 			= "ci"
	node_count					= 1
	node_boot_disk_size			= 5
	node_nat					= true
	node_loc_subnet_id			= module.network.proj_loc_subnet[0].id
	node_loc_ip					= ["192.168.0.10"]
	node_tcp_healthcheck_port 	= 22
}

module "srv-test" {

	source						= "./modules/server"

	yc_zone						= var.yc_zone

	srv_dns_domain				= module.dns.proj_domain
	srv_dns_record				= ["test"]
	srv_ext_address				= module.network.proj_ext_ip[0].external_ipv4_address.0.address
	srv_tcp_listen_port 		= 8080

	node_platform_id 			= "standard-v3"
	node_name_preffix 			= "test"
	node_count					= 1
	node_boot_disk_size			= 5
	node_nat					= true
	node_loc_subnet_id			= module.network.proj_loc_subnet[0].id
	node_loc_ip					= ["192.168.0.15"]
	node_tcp_healthcheck_port 	= 22
}


module "srv-monitoring" {

	source						= "./modules/server"

	yc_zone						= var.yc_zone

	srv_dns_domain				= module.dns.proj_domain
	srv_dns_record				= ["monitoring"]
	srv_ext_address				= module.network.proj_ext_ip[1].external_ipv4_address.0.address
	srv_tcp_listen_port 		= 3000

	node_platform_id 			= "standard-v3"
	node_name_preffix 			= "monitoring"
	node_count					= 1
	node_boot_disk_size			= 7
	node_nat					= true
	node_loc_subnet_id			= module.network.proj_loc_subnet[0].id
	node_loc_ip					= ["192.168.0.20"]
	node_tcp_healthcheck_port 	= 22
}

module "srv-prod" {

	source						= "./modules/server"

	yc_zone						= var.yc_zone

	srv_dns_domain				= module.dns.proj_domain
	srv_dns_record				= ["@"]
	srv_ext_address				= module.network.proj_ext_ip[3].external_ipv4_address.0.address
	srv_tcp_listen_port 		= 8080

	node_platform_id 			= "standard-v3"
	node_name_preffix 			= "prod"
	node_count					= 2
	node_boot_disk_size			= 5
	node_nat					= true
	node_loc_subnet_id			= module.network.proj_loc_subnet[0].id
	node_loc_ip					= ["192.168.0.30", "192.168.0.31"]
	node_tcp_healthcheck_port 	= 22
}

