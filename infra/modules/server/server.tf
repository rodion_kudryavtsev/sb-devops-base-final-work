terraform {
    required_providers {
        yandex = {
            source = "yandex-cloud/yandex"
        }
    }
}
provider "yandex" {
    zone = var.yc_zone
}
#===========================================

resource "yandex_compute_instance" "compute_instance" {

    count       				= var.node_count
    name        				= "${var.node_name_preffix}-${count.index + 1}"
    hostname        			= "${var.node_name_preffix}-${count.index + 1}"
    platform_id 				= var.node_platform_id
    allow_stopping_for_update 	= true
    resources {
        core_fraction 			= 20
        cores  					= 2
        memory 					= 2
    }
    boot_disk {
        initialize_params {
            image_id 			= data.yandex_compute_image.ubuntu-2004-lts.id
            size				= var.node_boot_disk_size
        }
    }

    network_interface {
        ipv6 					= false
        subnet_id				= var.node_loc_subnet_id
        ip_address				= var.node_loc_ip[count.index]
        nat       				= var.node_nat
    }

    metadata = {
        serial-port-enable 		= "0"
        user-data 				= "${file("./user_ansible")}"
    }
}

resource "yandex_lb_target_group" "lb_target_group" {

    name      = "${var.node_name_preffix}-target-group"
    dynamic "target" {
        for_each = yandex_compute_instance.compute_instance
        content {
            subnet_id = target.value.network_interface.0.subnet_id
            address = target.value.network_interface.0.ip_address
        }
    }
}

resource "yandex_lb_network_load_balancer" "lb_network_load_balancer" {
	count 	= length(var.srv_dns_record)
    name = "${var.node_name_preffix}-load-balancer"
    listener {
        name = "${var.node_name_preffix}-listener"
        port = var.srv_tcp_listen_port
        target_port = var.srv_tcp_listen_port
        external_address_spec {
            ip_version = "ipv4"
            address = var.srv_ext_address
        }
    }
    attached_target_group {
        target_group_id = yandex_lb_target_group.lb_target_group.id
        healthcheck {
            name = "tcp"
            tcp_options {
                port = var.node_tcp_healthcheck_port
            }
        }
    }
}

resource "yandex_dns_recordset" "recordset" {
	count 	= length(var.srv_dns_record)
    zone_id = var.srv_dns_domain.id
    name    = var.srv_dns_record[count.index]
    type    = "A"
    ttl     = 200
    data    = [var.srv_ext_address]
}

