#--- определены в корневом модуле ---

variable "yc_zone" {
}

#--- определены для этого модуля ---

variable "srv_ext_address" {
	description = "Публичный адрес сервера"
	default		= null
	type		= string
}
variable "srv_tcp_listen_port" {
	description = "Слушаемый tcp-порт сервера"
	default		= -1
	type		= string
}
variable "srv_dns_record" {
	description = "DNS-запись"
	default		= []
	type		= list
}
variable "srv_dns_domain" {
	description = "DNS-домен"
	default		= null

}


variable "node_name_preffix" {
	description = "Префикс в имени ноды сервера"
	default		= "noname"
	type		= string
}
variable "node_count" {
	description = "Кол-во нод в сервере"
	default		= 0
	type		= string
}
variable "node_platform_id" {
	description = "Параметры создаваемой ВМ"
	default		= "standard-v3"
	type		= string
}
variable "node_nat" {
	default = false
	type = bool
}
variable "node_loc_subnet_id" {
	default = null
	type = string
}
variable "node_loc_ip" {
	default = []
	type = list
}
variable "node_boot_disk_size" {
	description = "Размер диска (Gb)"
	default		= 5
	type		= string
}
variable "node_tcp_healthcheck_port" {
	description = "Healthcheck tcp-порт сервера"
	default		= 22
	type		= string
}

