terraform {
    required_providers {
        yandex = {
            source = "yandex-cloud/yandex"
        }
    }
}
provider "yandex" {
    zone = var.yc_zone
}

#=============================================

resource "yandex_dns_zone" "dns_zone" {
    zone             = var.proj_dns_zone
    public           = true
}
