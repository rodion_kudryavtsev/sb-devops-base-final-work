#--- определены в корневом модуле ---

variable "yc_zone" {
}

#--- определены для этого модуля ---

variable "net_name" {
	description = "Имя сети"
	default		= "net"
	type		= string
}
variable "proj_loc_subnets" {
	description = "Подсети"
	default		= []
	type		= list
}
variable "proj_ext_ip_count" {
	description = "Кол-во выделяемых публичных адресов"
	default		= 0
	type		= string
}
