output "proj_ext_ip" {
	value = yandex_vpc_address.ext_ipv4
}

output "proj_loc_subnet" {
	value = yandex_vpc_subnet.vpc_subnet
}
