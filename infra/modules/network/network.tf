terraform {
    required_providers {
        yandex = {
            source = "yandex-cloud/yandex"
        }
    }
}
provider "yandex" {
    zone = var.yc_zone
}

#===========================================================

resource "yandex_vpc_network" "vpc_network" {
    name = var.net_name
}

resource "yandex_vpc_subnet" "vpc_subnet" {
	count 			= length(var.proj_loc_subnets)
    name       		= "${var.net_name}-${count.index + 1}"
    v4_cidr_blocks 	= [var.proj_loc_subnets[count.index]]
    zone       		= var.yc_zone
    network_id 		= yandex_vpc_network.vpc_network.id
}

resource "yandex_vpc_address" "ext_ipv4" {
	count 			= var.proj_ext_ip_count
    name 			= "public_ip-${count.index + 1}"
    external_ipv4_address {
        zone_id 	= var.yc_zone
    }
}
